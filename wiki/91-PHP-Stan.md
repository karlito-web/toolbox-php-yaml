# PHP Stan
#### by Karlito Web

***

[Documentation](https://phpstan.org/config-reference)

### Install
``` shell
composer require --dev phpstan/phpstan
```

### Usage
Analyse
``` shell
php vendor\bin\phpstan analyse --memory-limit 2G
```

Analyse with Export file
``` shell
php vendor\bin\phpstan analyse --memory-limit 2G --error-format=table > PHPStan.txt
```

Clear Cache
``` shell
php vendor\bin\phpstan clear-result-cache
```
