<?php declare(strict_types=1);

namespace KarlitoWeb\Toolbox\Yaml\tests;

use KarlitoWeb\Toolbox\Yaml\YamlToArray;
use PHPUnit\Framework\TestCase;

class YamlToArrayTest extends TestCase
{
    public function testReturnArray()
    {
        $yaml = new YamlToArray();
        $r = $yaml::generate('documents/example.yaml');
        $this->assertIsArray($r);
    }
}
