# Usage
#### by Karlito Web

***

To convert YAML to PHP Array
``` php  
use KarlitoWeb\Toolbox\Yaml\YamlToArray;

$fn = new YamlToArray();

$array = $fn::generate($filepath);
```

To save PHP Array in YAML File
``` php  
use KarlitoWeb\Toolbox\Yaml\ArrayToYaml;

$fn = new ArrayToYaml();

$string = $fn::ArrayToYaml($filepath, $content, $regenerate);  
```