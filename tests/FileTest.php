<?php declare(strict_types=1);

namespace KarlitoWeb\Toolbox\Yaml\tests;

use PHPUnit\Framework\TestCase;

class FileTest extends TestCase
{
    protected static string $directory = 'E:/PHP/Toolbox/php-yaml/documents/';

    protected static string $file = '/example.yaml';

    public function testDirectoryExists(): void
    {
        $this->assertDirectoryExists(self::$directory, "directoryPath exists");
    }

    public function testDirectoryDoesNotExist(): void
    {
        $this->assertDirectoryDoesNotExist('/path', "directoryPath does not exists");
    }

    public function testDirectoryIsReadable(): void
    {
        $this->assertDirectoryIsReadable(self::$directory);
    }

    public function testFileExist(): void
    {
        $this->assertFileExists(self::$directory . self::$file);
    }

    public function testFileIsReadable(): void
    {
        $this->assertFileIsReadable(self::$directory . self::$file);
    }
}
