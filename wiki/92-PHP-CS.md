# PHP Coding Standards Fixer
#### by Karlito Web

***

[Documentation](https://cs.symfony.com/)

### Install
``` shell
composer require --dev friendsofphp/php-cs-fixer
```

### Usage
``` shell
php vendor/bin/php-cs-fixer fix src --diff

php vendor/bin/php-cs-fixer fix src

php vendor/bin/php-cs-fixer fix /path/to/file
php vendor/bin/php-cs-fixer fix /path/to/dir
php vendor/bin/php-cs-fixer fix /path/to/dir --rules=line_ending,full_opening_tag,indentation_type
php vendor/bin/php-cs-fixer fix /path/to/dir --rules=-full_opening_tag,-indentation_type
php vendor/bin/php-cs-fixer fix /path/to/dir --rules=@Symfony,-@PSR1,-blank_line_before_statement,strict_comparison

php vendor/bin/php-cs-fixer list-files
```

### IDE
[PHPStorm](https://www.jetbrains.com/help/phpstorm/using-php-cs-fixer.html#installing-configuring-php-cs-fixer)  
[Sublime-Text](https://github.com/benmatselby/sublime-phpcs)