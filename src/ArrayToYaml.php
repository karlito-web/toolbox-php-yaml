<?php

declare(strict_types=1);

namespace KarlitoWeb\Toolbox\Yaml;

use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml as Symfony;

/**
 * @author      Karlito15                               <giancarlo.palumbo@free.fr>
 * @license     https://opensource.org/license/mit/     MIT
 * @link        https://spacelift.io/blog/yaml          Documentation of Yaml language.
 * @package     karlito-web/toolbox-php-yaml
 * @subpackage  symfony/yaml
 * @version     3.0.0
 */
class ArrayToYaml
{
    /**
     * Convert an array to a yaml file.
     *
     * @param string $filepath  The path to the YAML file to be parsed
     * @param array $content
     * @param bool $regenerate
     * @return string
     */
    public static function generate(string $filepath, array $content, bool $regenerate = true): string
    {
        try {
            $yaml = Symfony::dump($content);

            if ($regenerate) {
                file_put_contents($filepath, $yaml);
            } else {
                file_put_contents($filepath, $yaml, FILE_APPEND);
            }

            return $yaml;
        } catch (ParseException $exception) {
            throw new ParseException($exception->getMessage());
        }
    }
}
