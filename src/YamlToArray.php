<?php

declare(strict_types=1);

namespace KarlitoWeb\Toolbox\Yaml;

use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml as Symfony;

/**
 * @author      Karlito15 <giancarlo.palumbo@free.fr>
 * @license     https://opensource.org/license/mit/ MIT
 * @link        https://spacelift.io/blog/yaml Documentation of Yaml language.
 * @package     karlito-web/toolbox-php-yaml
 * @subpackage  symfony/yaml
 * @version     3.0.0
 */
class YamlToArray
{
    /**
     * Parses a YAML file into a PHP value.
     *
     * @param string $filepath  The path to the YAML file to be parsed
     * @param int $flags        A bit field of PARSE_* constants to customize the YAML parser behavior
     * @return mixed            The YAML converted to a PHP value
     */
    public static function generate(string $filepath, int $flags = 0): mixed
    {
        $return = [];
        try {
            $return = Symfony::parseFile($filepath, $flags);
        } catch (ParseException $exception) {
            throw new ParseException($exception->getMessage());
        } finally {
            return $return;
        }
    }
}
