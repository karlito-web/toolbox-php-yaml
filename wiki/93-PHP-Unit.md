# PHP Unit
#### by Karlito Web

***

[Documentation](https://docs.phpunit.de/en/10.2/index.html)

### Install
``` shell
composer require --dev phpunit/phpunit
```

### Usage
``` shell
php vendor/bin/phpunit tests
```